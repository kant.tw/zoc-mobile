$(function() {

	var offCanvas = {
		init: function() {
			$('.mainMenu').find('.subMenu').each(function() {
				$(this).data('menuheight', $(this).height());
				$(this).height(0);
				$(this).css({opacity: 1});
			});
			$('.subMenuTrigger').each(function() {
				$(this).data('originheight', $(this).height());
			});
			this.bindEvents();
		}, 

		accTrigger: function() {
			$('.subMenuTrigger').on('click', function(e) {
				e.preventDefault();
				
					var targetHeight = $(this).next('.subMenu').data('menuheight');
					// $(this).css({'display': 'none'});
					$(this).animate({'height': 0}, 600);
					$(this).parent('li').addClass('active');
					$(this).next('.subMenu').animate({height: targetHeight}, 600, 'easeInOutQuad');
					//close others
					var siblings = $(this).parent('li').siblings();
					siblings.each(function() {
						var siblingTitleHeight = $(this).find('.subMenuTrigger').data('originheight');
						$(this).removeClass('active');
						$(this).find('.subMenuTrigger').animate({height: siblingTitleHeight}, 600, 'easeInOutQuad');
						$(this).find('.subMenu').animate({height: 0}, 600, 'easeInOutQuad');
					});
					// siblings.removeClass('active');
					// siblings.find('.subMenu').animate({height: 0}, 600, 'easeInOutQuad');
				
			});
		},

		backTrigger: function() {
			$('.subMenu_back').on('click', function(e) {
				e.preventDefault();
				var theLi = $(this).parents('li');
				var titleHeight = theLi.find('.subMenuTrigger').data('originheight');
				console.log('back ' + titleHeight);
				$(this).parents('li').removeClass('active');
				// theLi.find('.subMenuTrigger').css('display', 'block');
				theLi.find('.subMenuTrigger').animate({'height': titleHeight}, 600);
				$(this).parents('.subMenu').animate({height: 0}, 600, 'easeInOutQuad');
			});
		},

		toggleTrigger: function() {
			$('header .menu').on('click', function(e) {
				e.preventDefault();
				$('body').addClass('menuOpened');
			});
		},

		closeTrigger: function() {
			$('.menuClose').on('click', function(e) {
				e.preventDefault();
				$('body').removeClass('menuOpened');
			});
		},

		// styleTrigger: function() {
		// 	$('.submenu a').on('click', function(e) {
		// 		$(this).toggleClass('active');
		// 	});
		// },

		bindEvents: function() {
			this.toggleTrigger();
			this.accTrigger();
			this.closeTrigger();
			this.backTrigger();
			// this.styleTrigger();
		}
	}
	offCanvas.init();

});
